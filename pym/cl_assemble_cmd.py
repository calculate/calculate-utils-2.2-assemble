#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from cl_assemble import cl_assemble, __app__, __version__
from cl_opt import opt
from cl_share_cmd import share_cmd
from cl_vars_share import varsShare
import os
from os import path, access, R_OK
import re
import sys

from cl_lang import lang
lang().setLanguage(sys.modules[__name__])

OSSYSTEM_LIST=sorted(varsShare.dictNameSystem.keys())
ARCHS=sorted(['i686','x86_64'])
#ARCHS=sorted(['x86_64','ia64','i686','i386','arm','mips','mips64'])

DESCRIPTION = _("Configure for assembling Calculate Linux")
CMD_OPTIONS = [{'shortOption':"d",
                'longOption':"disk",
                'optVal':"DISK",
                'help':_("partition or directory intended for assemble")
               },
               {'shortOption':"p",
                'longOption':"profile",
                'optVal':"PROFILE",
                'help':_("system profile ('list' to display all)")
               },
               {'longOption':"march",
                'optVal':"ARCH",
                'type':'choice',
                'choices':ARCHS,
                'help':_("select arch for the operation system") +
                       " (%s)"%_("%(list)s or %(last)s")%
                        {'list':",".join(ARCHS[:-1]),
                         'last':ARCHS[-1]}
               },
               {'longOption':"source",
                'optVal':"SOURCE",
                'help':_("base stage for assemble") + " " +
                       _("(stage or shortname)")
               },
               {'longOption':"ver",
                'optVal':"VER",
                'help':_("version of the built system")
               },
               {'longOption':"name",
                'optVal':"SHORTNAME[/NAME[/SUBNAME]]",
                'help':_("distribution name (shortname/full name/subname)")
               },
               {'longOption':"sync",
                'optVal':"RSYNC",
                'help':_("set the preferred mirror for Portage")
               },
               {'longOption':"unmount",
                'help':_("unmount the partition where the system"
                         " is being assembled, when an error occurs")
               },
               {'longOption':"no-buildpkg",
                'help':_("create no binary packages for all merged packages")
               },
               {'longOption':"skip-rebuild-world",
                'help':_("do not rebuild world file")
               },
               {'longOption':"set"},
               {'shortOption':"f",
                'longOption':"force",
                'help':_("no questions during install")
               },
               ]
USAGE = _("%prog [options]")

class assemble_cmd(share_cmd):
    """Class for work with cl_assemble by console"""
    def __init__(self):
        setpos = \
            filter(lambda x:x[1].get('longOption')=="set",
            enumerate(CMD_OPTIONS))[0][0]
        CMD_OPTIONS[setpos] = opt.variable_set[0]
        self.optobj = opt(package=__app__,
                          version=__version__,
                          description=DESCRIPTION,
                          usage=USAGE,
                          option_list= CMD_OPTIONS + opt.variable_view +
                          opt.color_control,
                          check_values=self.checkOpts)
        self.logicObj = cl_assemble()

    def checkChoice(self,option,value,values):
        """Check choice and raise error"""
        if not value.upper() in map(lambda x:x.upper(),values):
            self.optobj.error(_("option %s")%option+
                    ": "+
                    _("invalid choice")+": '%s'"%value +
                    " (%s)"%_("choose from %s")%
                    ", ".join(values))

    def checkOpts(self, values, args):
        """Check values all specified options."""
        if len(args) > 0:
            self.optobj.error(_("unrecognized option") + ": %s"% "".join(args))
        if values.march:
            self.logicObj.clVars.Set('os_assemble_arch_machine',
                                     values.march,True)
        self.setAction()
        if values.p:
            if not self.logicObj.setProfile(values.p,True):
                sys.exit(1)
        self.optobj.checkVarSyntax(values)
        self.setVars(values)
        if not values.v and self.isRoot(printError=False):
            rootDev = self.logicObj.clVars.Get('os_assemble_root_dev')
            if not self.logicObj._checkAlreadyAssembling(printERROR=False) \
                and not values.d and ( not rootDev or \
                rootDev in self.logicObj.clVars.Get('cl_assemble_dev')) and \
                not self.logicObj.tryUseAvailable():
                self.optobj.error(
                    _("partition must be specified with the '-d' option"))
        if values.d:
            self.logicObj.clVars.Set('os_assemble_root_dev',values.d,True)
        if values.sync:
            if not self.logicObj.setSyncMirror(values.sync):
                self.optobj.error(
                    _("wrong rsync mirror '%s'")%values.sync)
        if values.ver:
            self.logicObj.clVars.Set('os_assemble_linux_ver',
                                     values.ver,True)
        if values.source:
            validSources = map(lambda x:x.upper(),
                               self.logicObj.getValidSource())+["stage"]
            self.checkChoice("--source",values.source, validSources)
        if values.name:
            if not self.logicObj.setName(values.name):
                self.optobj.error(_("option %s")%"--name"+
                    ": "+ _("invalid value '%s'"%values.name))
        if values.skip_rebuild_world:
            self.logicObj.clVars.Set('cl_assemble_rebuild_world',
                                     "off",True)
        return (values, args)

    def isScratch(self):
        if self.logicObj.clVars.Get('os_scratch') == "on":
            self.printERROR(_("You cannot use cl-assemble in Scratch mode."))
            return True
        return False

    def setAction(self):
        """Set action by configuration or install system"""
        if self.optobj.values.no_buildpkg:
            self.logicObj.clVars.Set('cl_assemble_buildpkg_set',
                                     "off",True)
        self.logicObj.setAction("configure")
        if self.optobj.values.source:
            source = self.optobj.values.source.upper()
            if source == "STAGE":
                source = "stage"
            self.logicObj.clVars.Set('cl_assemble_source',
                        source,True)

    def configureSystem(self,force,nounmount):
        """Unpack stage and prepare for assemble"""
        if not self.logicObj.configureSystem(False,force,nounmount):
            return False
        return True

    def checkVariables(self,printVars=None):
        if not self.logicObj.checkVariables(printVars):
            return False
        return True
