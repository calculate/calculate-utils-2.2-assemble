#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0 #
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Available values of keys
#   mode -     read only or writeable variable
#   value    - default variable value
#   select   - list of posible values for variable
#   hide - flag, if it is True, then the variable is not printable
#   printval - print value of variable
from cl_assemble import __version__, __app__

class Data:
    # relative path for apply templates on files of system
    cl_root_path = {}

    # program name
    cl_name = {'value':__app__}

    # program version
    cl_ver = {'value':__version__}

    # current action for make process
    cl_assemble_make = {'value':""}

    # current assembling systems
    cl_assemble_distro = {}
    
    # used devices for system assembling
    cl_assemble_dev = {}
    # devices, which used for assembling by random
    cl_assemble_available_dev = {'mode':'w'}
    # step of make action
    # action: emerge -e system
    cl_assemble_step_system = {}
    # action: emerge --newuse -D world
    cl_assemble_step_newuse = {}
    # action: emerge -uD world
    cl_assemble_step_update = {}
    # action: emerge --noreplace
    cl_assemble_step_world = {}

    # preferred rsync mirror for portages
    cl_assemble_sync = {}

    # preferred git branch for portages
    cl_assemble_branch = {'mode':'w'}

    # type of source (stage of system name)
    cl_assemble_source = {}

    # FEATURES="buildpkg" for chroot commands
    cl_assemble_buildpkg_set = {}

    # fetch video drivers
    cl_assemble_withvideo_set = {'value':'off'}

    # add -a option for all emerge
    cl_assemble_ask_set = {'value':'off'}

    # add -a option emerge world and emerge -c
    cl_assemble_ask_important_set = {'value':'off'}

    # pid of assembling process
    cl_assemble_pid = {}

    # need perform templates for assemble:prepare
    ac_assemble_prepare = {}

    # list of available profiles
    cl_assemble_profile = {}

    # --skipfirst for emerge --resume
    cl_assemble_skipfirst_set = {'value':'off'}

    # meta package for building
    cl_assemble_meta = {'mode':'w'}

    # need perform templates for assemble:setup
    ac_assemble_setup = {}

    # system image for installation
    cl_assemble_image = {'mode':'w'}

    # type of stage (iso or tarball)
    cl_assemble_image_type = {}

    # path for snapshots
    cl_assemble_snapshot_path = {'value':['/var/calculate/remote/snapshots',
                                         '/usr/calculate/share/snapshots']}
    #  latest snapshot
    cl_assemble_snapshot_portage = {'mode':'w'}

    # path which contains images
    cl_assemble_image_path = {}

    # assemble path
    cl_assemble_path = {}

    # PKGDIR for binary packages
    cl_assemble_pkgdir = {}

    # distro directory
    cl_assemble_linuxdir = {}
    
    # linux version of assembling system
    os_assemble_linux_ver = {'mode':'w'}

    # subname of installation os
    os_assemble_linux_subname = {'mode':'w'}

    # shortname of installation os
    os_assemble_linux_shortname = {'mode':'w'}

    # name of installation os
    os_assemble_linux_name = {'mode':'w'}

    # installation os  system: server or desktop
    os_assemble_linux_system = {}

    # arch of assembling os
    os_assemble_arch_machine = {}

    # arch by gentoo
    os_assemble_gentoo_arch = {}

    # current profile
    os_assemble_system_profile = {}

    # real profile (if profile is binary, then binary discard)
    os_assemble_profile = {}

    # device for assembling
    os_assemble_root_dev = {}

    # filesystem format of assemble partition
    os_assemble_root_format = {'mode':'w'}

    # infomation about disk in hash
    os_device_hash = {'hide':True}

    # devices
    os_device_dev = {}

    # device type (hdd,cdrom,usb-flash)
    os_device_type = {}

    # list of available partition devices
    os_disk_dev = {}

    # makeconf makeopts
    os_assemble_makeopts = {'mode':'w'}

    # matrix for prepare driver script
    cl_assemble_drivers_info = {'mode':'r'}

    # part of script for driver installing
    cl_assemble_drivers_script = {'mode':'r'}

    # execute cl-core --method update --rebuild-world on assemble
    cl_assemble_rebuild_world = {'mode':'w',
                                 'value':'on'}
