#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
import os

from cl_print import color_print
from cl_utils import _error
from cl_assemble import AssembleError

# Application message translator
from cl_lang import lang
lang().setLanguage(sys.modules[__name__])
import cl_overriding

class share_cmd(color_print, _error):
    """Class of share methods for perform cmd line"""
    def isRoot(self, printError=True):
        """Detect root user"""
        if os.getuid() == 0 and os.getgid() == 0:
            return True
        else:
            if printError:
                self.printERROR(_("The user is not root"))
            return False

    def printVars(self, optObj):
        """Variable printing from cmdline"""
        if optObj.v:
            varsFilter = None
            varsNames = []
            format = "default"
            # variable filter
            if optObj.filter:
                optCmd = optObj.filter
                if ',' in optCmd:
                    varsNames = optCmd.split(",")
                else:
                    varsFilter = optCmd
            if optObj.xml:
                format = "xml"
            try:
                v = int(optObj.v)
            except:
                v = 1
            self.logicObj.clVars.printVars(varsFilter, varsNames, 
                                           outFormat=format,
                                           verbose=v)

    def setVars(self, optObj):
        """Variable setting from cmdline"""
        if optObj.set:
            for val in optObj.set:
                k,o,v = val.partition('=')
                if self.logicObj.clVars.exists(k):
                    if self.logicObj.clVars.SetWriteVar(k,v) == False:
                        self.printERROR(AssembleError().getAssembleErrors(),
                                        printBR=False)
                        return False
                else:
                    self.printERROR(_('variable %s not found')%k)
                    return False
        return True

    def writeVars(self, optObj):
        """Variable writing"""
        if not self.logicObj.clVars.WriteVars(header="install"):
            errMsg = self.getError()
            if errMsg:
                self.printERROR(errMsg.strip())
            self.printERROR(_("Cannot write template variables"))
            return False
        return True

    def setPrintNoColor(self, optObj):
        """Setting message printing without color"""
        if optObj.color and optObj.color=="never":
            color_print.colorPrint = lambda *arg : sys.stdout.write(arg[-1]) or\
                                                   sys.stdout.flush()

    def _getNamesAllSetOptions(self):
        """Get list set options"""
        setOptDict = self.optobj.values.__dict__.items()
        defaultOptDict = self.optobj.get_default_values().__dict__.items()
        return reduce(lambda x,y: x+[y[0][0]],
                      filter(lambda x:x[0][1] != x[1][1],
                             zip(setOptDict,defaultOptDict)), [])

    def getStringIncompatibleOptions(self,listOpt):
        """Formated string incompatible options which separated ','"""
        return ", ".join(map(lambda x: len(x) == 1 and "'-%s'"%x or "'--%s'"%x,
                             listOpt))

    def checkIncompatibleParam(self,param):
        """Check incompatible options for option specified by param"""
        incompatible = list(set(self._getNamesAllSetOptions()) &
                            set(getattr(self,"options%sIncompatible"%
                                param.capitalize().replace("-",""))))
        if incompatible:
            self.optobj.error(_("incompatible options")+":"+" %s"\
                    %self.getStringIncompatibleOptions(incompatible+[param]))
