#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from cl_assemble import cl_assemble, __app__, __version__
from cl_assemble_cmd import assemble_cmd
from cl_opt import opt
from cl_share_cmd import share_cmd
import os
from os import path, access, R_OK
import re
import sys

from cl_lang import lang
lang().setLanguage(sys.modules[__name__])

DESCRIPTION = _("Assembling Calculate Linux")
CMD_OPTIONS = [{'shortOption':"D",
                'longOption':"dependence",
                'help':_("check dependencies")
               },
               {'shortOption':"m",
                'longOption':"make",
                'help':_("build the system")
               },
               {'shortOption':"u",
                'longOption':"update",
                'help':_("update the system")
               },
               {'shortOption':"U",
                'longOption':"update-without-sync",
                'help':_("update the system without eix-sync")
               },
               {'shortOption':"T",
                'longOption':"apply-templates",
                'help':_("configure with %s templates")% "assemble"
               },
               {'longOption':"break",
                'help':_("break system assemble")
               },
               {'shortOption':"p",
                'longOption':"profile",
                'optVal':"PROFILE",
                'help':_("system profile ('list' to display all)")
               },
               {'longOption':"sync",
                'optVal':"PORTAGE",
                'help':_("set the preferred mirror for Portage")
               },
               {'longOption':"skipfirst",
                'help':_("skip the first package when resuming the assemble")
               },
               {'shortOption':'V',
                'longOption':"withvideo",
                'help':_("load video drivers onto the built system")
               },
               {'longOption':"set"},
               {'shortOption':"f",
                'longOption':"force",
                'help':_("no questions during install")
               },
               {'shortOption':"F",
                'help':_("ask only important questions")
               }]
USAGE = _("%prog [options] --dependence|--update|"
          "--update-without-sync|--make|--break")

class make_cmd(assemble_cmd):
    """Class for work with cl_assemble by console"""
    def __init__(self):
        setpos = \
            filter(lambda x:x[1].get('longOption')=="set",
            enumerate(CMD_OPTIONS))[0][0]
        CMD_OPTIONS[setpos] = opt.variable_set[0]
        self.optobj = opt(package=__app__,
                          version=__version__,
                          description=DESCRIPTION,
                          usage=USAGE,
                          option_list= CMD_OPTIONS + opt.variable_view +
                          opt.color_control,
                          check_values=self.checkOpts)
        self.logicObj = cl_assemble()
        self.optionsBreakIncompatible=["U","skipfirst","m","u","D","t"]
        self.optionsDependenceIncompatible=["U","skipfirst","m","u","break","t"]
        self.optionsMakeIncompatible=["U","D","u","break","t"]
        self.optionsUpdateIncompatible=["U","m","D","break","t"]
        self.optionsUpdatenosyncIncompatible=["D","m","u","break","t"]
        self.optionsApplytemplatesIncompatible=["D","m","u","break","U"]

    def checkOpts(self, values, args):
        """Check values all specified options."""
        if len(args) > 0:
            self.optobj.error(_("unrecognized option") + ": %s"% "".join(args))
        self.setAction()
        if values.p:
            if not self.logicObj.setProfile(values.p,False):
                sys.exit(1)
        self.optobj.checkVarSyntax(values)
        self.setVars(values)
        if values.V:
            self.logicObj.clVars.Set('cl_assemble_withvideo_set',"on",True)
        if not values.v and self.isRoot(printError=False):
            if values.m:
                self.checkIncompatibleParam("make")
            if getattr(values,"break"):
                self.checkIncompatibleParam("break")
            if values.D:
                self.checkIncompatibleParam("dependence")
            if values.u:
                self.checkIncompatibleParam("update")
            if values.U:
                self.checkIncompatibleParam("update-nosync")
            if values.T:
                self.checkIncompatibleParam("applytemplates")
            if not (values.m or getattr(values,"break") or
                    values.D or values.u or values.U or values.T):
                self.optobj.error(
                    _("action must be specified: make (-m), break (--break),"
                      " update (-u|-U), apply templates (-T)"
                      " or check dependencies (-D)"))
        if values.sync:
            if not self.logicObj.setSyncMirror(values.sync):
                self.optobj.error(
                    _("wrong rsync mirror '%s'")%values.sync)
        return (values, args)

    def setAction(self):
        """Set action by configuration or install system"""
        if getattr(self.optobj.values,"break"):
            action = "break"
        elif self.optobj.values.m:
            action = "make"
        elif self.optobj.values.u:
            action = "syncupdate"
        elif self.optobj.values.U:
            action = "update"
        elif self.optobj.values.T:
            action = "applytemplates"
        else:
            action = "dependence"
        self.logicObj.setAction(action)
        if action in ("make","syncupdate","update"):
            if self.optobj.values.skipfirst:
                self.logicObj.clVars.Set('cl_assemble_skipfirst_set',
                                         "on",True)

    def compileSystem(self,soft_force,force):
        """Compile all packages for system"""
        if not self.logicObj.makeSystem(soft_force,force):
            return False
        return True

    def updateSystem(self,soft_force,force,withsync=True):
        """Compile all packages for system"""
        if not self.logicObj.updateSystem(soft_force,force,withsync):
            return False
        return True

    def breakAssembling(self,soft_force,force):
        """Perform break assembling: unmount all resourse for assembling"""
        if not self.logicObj.breakAssembling(soft_force,force):
            return False
        return True

    def checkDependence(self):
        """Check dependence of assembling distributive"""
        if not self.logicObj.checkDependence():
            return False
        return True
